<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Event;

/**
 * EventSearch represents the model behind the search form of `app\models\Event`.
 */
class EventSearch extends Event {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'filter'], 'integer'],
            [['title', 'description', 'start_date', 'end_date', 'venue', 'created_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Event::find()->orderBy(['start_date' => SORT_ASC]);
        $date = date('Y-m-d');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'venue', $this->venue]);
        if ($this->filter == self::FINISHED) {
            $query->andFilterWhere(['<', 'end_date', $date]);
        } else if ($this->filter == self::UPCOMING) {
            $query->andFilterWhere(['>', 'end_date', $date]);
        } else if ($this->filter == self::UPCOMING_7D) {
            $filter_date = date('Y-m-d', strtotime('+7 days', strtotime($date)));
            $query->andFilterWhere(['between', 'start_date', $date, $filter_date]);
        } else if ($this->filter == self::FINISHED_7D) {
            $filter_date = date('Y-m-d', strtotime('-7 days', strtotime($date)));
            $query->andFilterWhere(['between', 'end_date', $filter_date, $date]);
        }
        



        return $dataProvider;
    }

}
