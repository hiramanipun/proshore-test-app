<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property string $venue
 * @property string $created_date
 */
class Event extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    // status of the event
    const FINISHED = 1;
    const UPCOMING = 2;
    const FINISHED_7D = 3;
    const UPCOMING_7D = 4;

    public $filter; // filter variable for events

    public function rules() {
        return [
            [['title', 'description', 'start_date', 'end_date', 'venue'], 'required'],
            [['description'], 'string'],
            [['start_date', 'end_date', 'created_date'], 'safe'],
            [['title', 'venue'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'venue' => 'Venue',
            'created_date' => 'Created Date',
            'filter' => 'Status',
        ];
    }

    public static function getFilters() {
        return [
            self::UPCOMING => 'Upcoming',
            self::FINISHED => 'Finished',
            self::UPCOMING_7D => 'Upcomming events within last 7 Days',
            self::FINISHED_7D => 'Finished events of last 7 Days',
        ];
    }

    public function getStatus() {
        if ($this->end_date < date('Y-m-d')) {
            return '<span class="badge" style="color:red;">Finished</p>';
        } else {
            return '<span class="badge" style="color:green;">Upcoming</p>';
        }
    }

}
